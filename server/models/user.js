const {mongoose}  = require('../db/mongoose')
const _validator  = require('validator')
const User = mongoose.model('User', {
    email: {
        type: String,
        required: true,
        trim: true,
        minlength:1,
        unique: true,
        validate: {
            validator(value){
                return _validator.isEmail(value)
            },
            // http://mongoosejs.com/docs/validation.html#custom-validators
            message: '{VALUE} is not a valid email'
        }
    },
    password: {
        type: String,
        required: true
    }
})

module.exports = { User }