const {mongoose}  = require('../db/mongoose')

const Todo = mongoose.model('Todo',{
    memo: {
        type: String,
        required: true,
        trim: true
    },
    completed: {
        type: Boolean,
        required: true
    }
})

module.exports = { Todo }