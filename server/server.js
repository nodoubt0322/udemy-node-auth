const app        = require('express')()
const bodyParser = require('body-parser')
const jwt        = require('jsonwebtoken')
const morgan     = require('morgan')
const bcrypt     = require('bcrypt')
const config     = require('./config')
const {User}     = require('./models/user')
const {Todo}     = require('./models/todo')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false}))
app.use(morgan('dev'))

app.post('/users',(req,res)=>{
    // https://github.com/kelektiv/node.bcrypt.js    
    const password = bcrypt.hashSync( req.body.password, 10);  
    const body = {
        email: req.body.email,
        password
    }
    const user = new User(body)
    user.save()
        .then((user)=>{
            res.send(user)
        })
        .catch((err)=>{
            res.status(400).send(err)
        })
})

app.post('/login', (req,res)=>{
    User.findOne({ email: req.body.email},
        (err, user)=>{
            if (err) throw err

            // 沒有找到user
            if (!user) {
                res.json({  
                    success: false,
                    message: 'Authentication failed, User not found'
                })
            }else if (bcrypt.compareSync(req.body.password, user.password )){
                // http://mongoosejs.com/docs/guide.html#toJSON
                // 使用toJSON()移除mongoose的method
                const token = jwt.sign(user.toJSON(), config.secret , {
                    expiresIn: 60*60*24
                })
                res.json({
                    success: true,
                    message: ' Authentication succeed',
                    token
                })                                
            } else {
                res.json({  
                    success: false,
                    message: 'Authentication failed, Wrong password'
                })
            }
        }
    )
})

//設定route middleware to check token
app.use((req, res, next)=>{
    const token = req.headers['token']

    if (token) {
        jwt.verify(token, config.secret, (err, decoded)=>{
            if (err){ res.json({
                    success: false,
                    message:'Failed to authenticate token'
                })
            }else {
                req.decoded = decoded
                next()
            }
        })
    }else{
        res.status(403).send({
            success: false,
            message: 'No token provided.'
        })
    }

})

app.post('/todos', (req, res)=>{
    const data = {
        memo: req.body.memo,
        completed: false
    }
    const todo = new Todo(data)
    todo.save()
        .then((todo)=>{
            res.send(todo)
        })
        .catch((err)=>{
            res.send(err)
        })
})

app.get('/todos', (req, res)=>{
    console.log(req.decoded)
    Todo.find()
        .then((todo)=>{
            res.send(todo)
        })
        .catch((err)=>{
            res.send(err)
        })
})


app.listen(3000, ()=>{
    console.log('server is running at port 3000')
})